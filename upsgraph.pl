#!/usr/bin/perl -w

#Due to memory leak in Debian squeeze (Bug #545519)
my $use_rrds = 0;
my $rrd_result = 0;

if ((@ARGV != 1) && (@ARGV != 2)) {
	print STDERR "Syntax: ${0} configfile [uid]\n";
	exit(1);
}

use Net::SNMP;
use IO::Socket::INET;
use IO::Select;
use RRDs;
use File::Copy;
use Data::Dumper;
use LWP::UserAgent;
use JSON;

$UPSGRAPH::outdir = "";
$UPSGRAPH::daysCovered = 370;
$UPSGRAPH::step = 60;
$UPSGRAPH::stepsPerHour = (60 * 60) / $UPSGRAPH::step;
$UPSGRAPH::keep = ($UPSGRAPH::daysCovered*24*60*60)/$UPSGRAPH::step;
$UPSGRAPH::keepHours = ($UPSGRAPH::daysCovered*24*60*60)/$UPSGRAPH::stepsPerHour/$UPSGRAPH::step;
$UPSGRAPH::hosts = ();
$UPSGRAPH::regenerateOnStart = 1; #when set, regenerate graphs on script startup

do $ARGV[0] or die "can't read config: $!";

my $outdir = $UPSGRAPH::outdir;
my $step = $UPSGRAPH::step;
my $stepsPerHour = $UPSGRAPH::stepsPerHour;
my $keep = $UPSGRAPH::keep;
my $keepHours = $UPSGRAPH::keepHours;
my $hosts = $UPSGRAPH::hosts;

sub rrd_update(@) {
	my @args = @_;

	if ($use_rrds == 1) {
		RRDs::update(@args);
		$rrd_result = RRDs::error;
	} else {
		$rrd_result = system("rrdtool", "update", @args);
	}
}

sub rrd_graph(@) {
	my @args = @_;
	my @rrd_out = ();

	if ($use_rrds == 1) {
		@rrd_out = RRDs::graph(@args);
		$rrd_result = RRDs::error;
	} else {
		my $rrd_stdout;

		open(RRDFD, '-|', 'rrdtool', 'graph', @args);
		while(<RRDFD>) {
			chomp;
			$rrd_stdout = $_;
		}
		close(RRDFD);
		$rrd_result = $?;
		if ($rrd_result == 0) {
			push @rrd_out, 0;
			push @rrd_out, split(/x/, $rrd_stdout);
		}
	}

	return @rrd_out;
}

sub rrdcreate(@) {
	my $newrrd = shift;
	my $field = shift;
	my $vars = shift;
	my $start = shift;

	my @cmd = ("${newrrd}", "--step=${step}");

	if (defined($start)) {
		push @cmd, "--start=${start}";
	}

	push @cmd, "DS:${field}:GAUGE:600:" .
		$vars->{$field}->{'min'} . ":" .
		$vars->{$field}->{'max'};

	push @cmd, "RRA:AVERAGE:0.5:1:${keep}";
	push @cmd, "RRA:MIN:0.4:${stepsPerHour}:${keepHours}";
	push @cmd, "RRA:MAX:0.4:${stepsPerHour}:${keepHours}";
	push @cmd, "RRA:AVERAGE:0.5:${stepsPerHour}:${keepHours}";

	RRDs::create(@cmd);
	if (RRDs::error) {
		print "Error while creating: " . RRDs::error . "\n";
		exit 1;
	}
}

sub fetch_snmp(@) {
	my $address = shift;
	my $community = shift;
	my $oid = shift;

	(my $session, my $error) = Net::SNMP->session(Hostname => $address,
			Community => $community);

	if (!$session) {
		print STDERR "session error: $error";
		return undef;
	}

	$session->translate(0);

	my $result = $session->get_request($oid);

	$session->close;

	return undef if (!defined($result));

	$result->{$oid};
}

sub fetch_tcp(@) {
	my $address = shift;
	my $port = shift;

	my $sock = IO::Socket::INET->new(PeerAddr => $address,
			PeerPort => $port,
			Proto => 'tcp',
			Timeout => 1);

	return undef if (!$sock);

	my $select = IO::Select->new($sock);

	my $value = undef;

	if ($select->can_read(1)) {
		chomp($value) if (sysread($sock, $value, 4096) > 0);
	}

	close($sock);

	if (!$value) {
		return undef;
	}

	$value=~ s/\s//g;

	$value;
}

sub fetch_tcp_multi(@) {
	my $address = shift;
	my $port = shift;
	my $delimiter = shift;
	my %values;

	my $sock = IO::Socket::INET->new(PeerAddr => $address,
			PeerPort => $port,
			Proto => 'tcp',
			Timeout => 1);

	return undef if (!$sock);

	my $select = IO::Select->new($sock);

	while($select->can_read(1)) {
		if (sysread($sock, my $buf, 16384) > 0) {
			$buf=~s/\r//g;
			foreach my $line (split(/\n/, $buf)) {
				(my $key, my $value) = split(/${delimiter}/, $line);
				$value=~ s/\s//g;
				$values{$key} = $value;
			}
		} else {
			last;
		}
	}

	close($sock);

	%values;
}

sub fetch_iotcore(@) {
	my $host = shift;
	my $adr = shift;
	my $mask = shift;

	my $ua = LWP::UserAgent->new;
	$ua->timeout(1);

	my $iotcore_req = {
		cid => 1,
		code => 10,
		adr => $adr,
	};

	my $req = HTTP::Request->new(POST => "http://${host}");
	$req->content_type('application/json');
	$req->content(encode_json($iotcore_req));

	my $resp = $ua->request($req);
	return undef if (!$resp->is_success);

	my $pdin = decode_json($resp->decoded_content);
	return undef if (!defined($pdin));

	my $value = hex($pdin->{'data'}->{'value'});

	if (defined($mask)) {
		$value = $value & $mask;
	}

	$value;
}

sub dayGraphFunc {
	my $dataSrc = shift;
	my $mode = shift;
	my $color = shift;
	my $label = shift;
	my $dataPoints = shift;
	my @args = ();
	push @args, "CDEF:prev${mode}1=PREV(${dataSrc})";
	for (my $i = 1; $i < $dataPoints - 1; ++$i) {
		my $prev = $i;
		my $next = $i+1;
		push @args, "CDEF:prev${mode}${next}=PREV(prev${mode}${prev})";
	}
	my $dayCons = '';
	my $consFunc = '';
	if ($mode ne 'AVG') {
		for (my $i = 1; $i < $dataPoints; ++$i) {
			$dayCons .= "prev${mode}${i},";
			$consFunc .= ",${mode}";
		}
	} else {
		for (my $i = 1; $i < $dataPoints; ++$i) {
			$dayCons .= "prev${mode}${i},";
		}
		$consFunc = ",${dataPoints},${mode}";
	}
	push @args, "CDEF:day${mode}=${dayCons}${dataSrc}${consFunc}";
	push @args, "CDEF:fillCalDay${mode}0=COUNT,${dataPoints},%,0,EQ,day${mode},UNKN,IF";
	for (my $i = 1; $i < $dataPoints; ++$i) {
		my $prev = $i-1;
		my $next = $i;
		push @args, "CDEF:fillCalDay${mode}${next}=PREV(fillCalDay${mode}${prev})";
	}
	my $fillPoint = '';
	my $if = '';
	for (my $i = 0; $i < $dataPoints; ++$i) {
		$fillPoint .= "COUNT,${dataPoints},%,${i},EQ,fillCalDay${mode}${i},";
		$if .= ",IF";
	}
	push @args, "CDEF:${mode}Curve=${fillPoint}UNKN${if}";
	my $forwardShift = (24*60*60) * ($dataPoints - 1) / $dataPoints;
	push @args, "SHIFT:${mode}Curve:-${forwardShift}";
	push @args, "LINE1:${mode}Curve#${color}:${label}";
	return \@args;
}

sub cfgToGraphDef {
	my $cfg = shift;
	my $varname = shift;
	my $dpPerDay= shift;
	my @graphDef = ();
	foreach my $subGraph (@$cfg) {
		if ($subGraph eq 'avg') {
			push @graphDef, "LINE1:${varname}-avg#FF0000";
		} elsif ($subGraph eq 'day-min') {
			push @graphDef, @{dayGraphFunc("${varname}-min", 'MIN', '0000ff', 'Day Minimum Temperature', $dpPerDay)};
		} elsif ($subGraph eq 'day-max') {
			push @graphDef, @{dayGraphFunc("${varname}-max", 'MAX', '00ff00', 'Day Maximum Temperature', $dpPerDay)};
		} elsif ($subGraph eq 'day-avg') {
			push @graphDef, @{dayGraphFunc("${varname}-houravg", 'AVG', 'ff0000', 'Day Average Temperature', $dpPerDay)};
		}
	}
	return \@graphDef;
}

if ($> == 0) {
	if (@ARGV != 2) {
		print STDERR "Running as root, please provide UID as 2th argument!\n";
		exit(1);
	}

	print "Running as root, switching to ".$ARGV[1]."\n";
	$< = $> = $ARGV[1];
}

foreach my $host (@$hosts) {
	my $rrdfile = $host->{'rrdfile'};

	foreach my $var (keys(%{$host->{'vars'}})) {
		$host->{'vars'}->{$var}->{'min'} = 'U' if (!defined($host->{'vars'}->{$var}->{'min'}));
		$host->{'vars'}->{$var}->{'max'} = 'U' if (!defined($host->{'vars'}->{$var}->{'max'}));
	}

	if (-e "${rrdfile}") {
		print "Reading old ${rrdfile} to preserve data...\n";

		my $rrdinfo = RRDs::info("${rrdfile}");
		if (RRDs::error) {
			print "Error while getting info: " . RRDs::error . "\n";
			exit 1;
		}

		(my $start, my $ostep, my $names, my $data) =
			RRDs::fetch("${rrdfile}",
					"-s " . (time() - ($rrdinfo->{'rra[0].rows'} * $rrdinfo->{'step'})),
					"AVERAGE");

		if (RRDs::error) {
			print "Error while fetching data: " . RRDs::error . "\n";
			exit 1;
		}

		foreach my $field (@$names) {
			if (! -e "${rrdfile}.${field}") {
				rrdcreate("${rrdfile}.${field}",
					"${field}",
					$host->{'vars'},
					(${start}-${ostep}));
			}
		}

		my $pos = $start;
		foreach my $line (@$data) {
			foreach my $field (@$names) {
				my $val = shift (@$line);
				$val = 'U' if (!defined($val));

				RRDs::update("${rrdfile}.${field}", "${pos}:${val}");
				if (RRDs::error) {
					print "Can't insert data: " . RRDs::error . "\n";
					exit 1;
				}

			}

			$pos += $ostep;

			if ((($pos-$start)/$ostep) == $#$data) {
				last;
			}
		}

		rename("${rrdfile}", "${rrdfile}.old") or die "Can't rename old file: $!\n";
	}

	foreach my $field (@{$host->{'fields'}}) {
		if (! -e "${rrdfile}.${field}") {
			print "Creating ${rrdfile}.${field}...\n";
			rrdcreate("${rrdfile}.${field}",
				"${field}",
				$host->{'vars'});
		}

		my $rrdinfo = RRDs::info("${rrdfile}.${field}");
		if (RRDs::error) {
			print "Error while getting info: " . RRDs::error . "\n";
			exit 1;
		}

		my $limitsChanged = 0;
		if (defined($rrdinfo->{"ds[${field}].min"})) {
			if ($rrdinfo->{"ds[${field}].min"} ne $host->{'vars'}->{$field}->{'min'}) {
				$limitsChanged = 1;
				RRDs::tune("${rrdfile}.${field}","-i",$field.":".$host->{'vars'}->{$field}->{'min'});
			}
		} else {
			if ($host->{'vars'}->{$field}->{'min'} ne 'U') {
				$limitsChanged = 1;
				RRDs::tune("${rrdfile}.${field}","-i",$field.":".$host->{'vars'}->{$field}->{'min'});
			}
		}

		if (RRDs::error) {
			print "Error while setting min: " . RRDs::error . "\n";
			exit 1;
		}

		if (defined($rrdinfo->{"ds[${field}].max"})) {
			if ($rrdinfo->{"ds[${field}].max"} ne $host->{'vars'}->{$field}->{'max'}) {
				$limitsChanged = 1;
				RRDs::tune("${rrdfile}.${field}","-a",$field.":".$host->{'vars'}->{$field}->{'max'});
			}
		} else {
			if ($host->{'vars'}->{$field}->{'max'} ne 'U') {
				$limitsChanged = 1;
				RRDs::tune("${rrdfile}.${field}","-a",$field.":".$host->{'vars'}->{$field}->{'max'});
			}
		}

		if (RRDs::error) {
			print "Error while setting max: " . RRDs::error . "\n";
			exit 1;
		}
		    
		if ($rrdinfo->{'rra[0].rows'} != $keep ||
			!defined($rrdinfo->{'rra[3].rows'}) || $rrdinfo->{'rra[3].rows'} != $keepHours ||
			$limitsChanged == 1) {

			print "Resizing ${rrdfile}.${field} from " . $rrdinfo->{'rra[0].rows'} .
				" to ${keep} samples.\n";

			(my $start, my $ostep, my $names, my $data) =
				RRDs::fetch("${rrdfile}.${field}",
						"-s " . (time() - ($rrdinfo->{'rra[0].rows'} * $rrdinfo->{'step'})),
						"AVERAGE");

			if (RRDs::error) {
				print "Error while fetching data: " . RRDs::error . "\n";
				exit 1;
			}

			rrdcreate("${rrdfile}.${field}.new",
				"${field}",
				$host->{'vars'},
				(${start}-${ostep}));

			print "Preserving data in file ${rrdfile}.${field} since " . localtime($start) . "\n";

			my $pos = $start;
			foreach my $line (@$data) {
				my $vline = "${pos}";

				foreach my $val (@$line) {
					$val = 'U' if (!defined($val));
					$vline .= ":${val}";
				}
				RRDs::update("${rrdfile}.${field}.new", $vline) or die "Can't insert data\n";

				if (RRDs::error) {
					print "Error while updating: " . RRDs::error . "\n";
					exit 1;
				}
				$pos += $ostep;

				if ((($pos-$start)/$ostep) == $#$data) {
					last;
				}
			}

			rename("${rrdfile}.${field}", "${rrdfile}.${field}.old") or die "Can't rename old file: $!\n";
			rename("${rrdfile}.${field}.new", "${rrdfile}.${field}") or die "Can't rename new file: $!\n";

			$rrdinfo = RRDs::info("${rrdfile}.${field}");
			if (RRDs::error) {
				print "Error while getting info: " . RRDs::error . "\n";
				exit 1;
			}

			if ($rrdinfo->{'rra[0].rows'} != $keep) {
				print "Failed!\n";
				exit 1;
			}
		}
	}
}

my $child = fork();

die "fork failed!" if (!defined($child));

exit 0 if ($child != 0);

my $first = $UPSGRAPH::regenerateOnStart;
while(1) {
	open(HTML, ">${outdir}/index.html.new");

	print HTML '<html><head><meta http-equiv="refresh" content="60"/><meta http-equiv="cache-control" content="no-cache"/><meta http-equiv="pragma" content="no-cache"/><meta http_equiv="expires" content="Sat, 26 Jul 1997 05:00:00 GMT"/><title>Status</title></head>';
	print HTML '<body bgcolor="#ffffff">';

	foreach my $host (@$hosts) {
		print HTML "[<a href=\"#".${host}->{'name'}."\">".${host}->{'name'}."</a>]&nbsp;";
	}
	print HTML "<br>\n";

	foreach my $host (@$hosts) {
		print HTML "<br>\n";
		print HTML "<a name=\"".${host}->{'name'}."\"></a>\n";
		my $vars = $host->{'vars'};
		my $rrdfile = $host->{'rrdfile'};
		my $hostname = $host->{'name'};
		my %multi_values = ();

		foreach my $var (@{$host->{'fields'}}) {
			delete $vars->{$var}->{'value'};

			my $result;

			if ((!defined($vars->{$var}->{'proto'})) ||
			    ($vars->{$var}->{'proto'} eq '') ||
			    ($vars->{$var}->{'proto'} eq 'snmp')) {
				$result = fetch_snmp($host->{'address'}, $host->{'community'}, $vars->{$var}->{'oid'});
			} elsif ($vars->{$var}->{'proto'} eq 'tcp') {
				$result = fetch_tcp($host->{'address'}, $vars->{$var}->{'port'});
			} elsif ($vars->{$var}->{'proto'} eq 'tcp_multi') {
				if (defined($multi_values{$vars->{$var}->{'multi_id'}})) {
					$result = $multi_values{$vars->{$var}->{'multi_id'}}
				} else {
					my %values = fetch_tcp_multi($host->{'address'}, $vars->{$var}->{'port'}, $vars->{$var}->{'multi_delimiter'});
					@multi_values{keys %values} = values %values;
					$result = $multi_values{$vars->{$var}->{'multi_id'}};
				}
			} elsif ($vars->{$var}->{'proto'} eq 'iotcore') {
				$result = fetch_iotcore($host->{'address'}, $vars->{$var}->{'adr'}, $vars->{$var}->{'mask'});
			}

			next unless (defined $result);

			$vars->{$var}->{'value'} = $result;
			if (defined($vars->{$var}->{'factor'})) {
				$vars->{$var}->{'value'} *= $vars->{$var}->{'factor'};
			}
		}

		foreach my $var (@{$host->{'fields'}}) {
			if (!(defined($vars->{$var}->{'value'}))) {
				$vars->{$var}->{'value'} = 'U';
			}
			rrd_update("${rrdfile}.${var}", "N:" . $vars->{$var}->{'value'});
		}
		if ($rrd_result) {
			print "Error while updating: " . $rrd_result . "\n";
		}

		foreach my $var (@{$host->{'fields'}}) {
			my $graphWidth = 365 * 3;
			my $graphConfig;
			if (defined $vars->{$var}->{'graph'}) {
				$graphConfig = $vars->{$var}->{'graph'};
				foreach my $subGraph (qw(day week year)) {
					if (!defined($graphConfig->{$subGraph})) {
						$graphConfig->{$subGraph} = [ 'avg'];
					}
				}
			} else {
				$graphConfig = {
					'day' =>  [ 'avg' ],
					'week' => [ 'avg' ],
					'year' => [ 'avg' ],
				};
			}

			my @graphdef = ('-P', "-A", "-t", $hostname." - ".$vars->{$var}->{'name'});
			push @graphdef, "--lazy" if !$first;

			push @graphdef, "DEF:${var}-avg=${rrdfile}.${var}:${var}:AVERAGE";
			push @graphdef, "DEF:${var}-min=${rrdfile}.${var}:${var}:MIN";
			push @graphdef, "DEF:${var}-max=${rrdfile}.${var}:${var}:MAX";
			push @graphdef, "DEF:${var}-houravg=${rrdfile}.${var}:${var}:AVERAGE:step=3600";
			push @graphdef, "LINE1:${var}-avg#FF0000";
			push @graphdef, "VDEF:cur=${var}-avg,LAST";
			push @graphdef, 'GPRINT:cur:Current\\: <span foreground="#FF0000">%.2lf</span>\\r';

			my $mtime;
			$mtime=(stat("${outdir}/${hostname}.${var}.png.work"))[9];

			(my $averages, my $width, my $height) =
				rrd_graph("${outdir}/${hostname}.${var}.png.work",
						"-w", "720", @graphdef);

			pop @graphdef;
			pop @graphdef;
			pop @graphdef;

			if ($rrd_result) {
				print "Error while graphing: " . $rrd_result . "\n";
			} else {
				my $newmtime=(stat("${outdir}/${hostname}.${var}.png.work"))[9];
				if ((!defined($mtime)) || ($newmtime != $mtime)) {
					copy("${outdir}/${hostname}.${var}.png.work", "${outdir}/${hostname}.${var}.png.new");
					rename("${outdir}/${hostname}.${var}.png.new", "${outdir}/${hostname}.${var}.png");
				}
			}

			print HTML "<a href=\"${hostname}.${var}.html\"><img src=\"${hostname}.${var}.png\" width=\"${width}\" height=\"${height}\" border=\"0\"></a><br>\n";

			open (HTML2, ">${outdir}/${hostname}.${var}.html.new");
			print HTML2 '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta http-equiv="refresh" content="60"/><meta http-equiv="cache-control" content="no-cache"/><meta http-equiv="pragma" content="no-cache"/><meta http_equiv="expires" content="Sat, 26 Jul 1997 05:00:00 GMT"/><title>' . $vars->{$var}->{'name'} . '</title></head>';
			print HTML2 '<body bgcolor="#ffffff">';

			push @graphdef, "VDEF:min=${var}-min,MINIMUM";
			push @graphdef, "GPRINT:min:Minimum\\: %.2lf";

			push @graphdef, "VDEF:avg=${var}-avg,AVERAGE";
			push @graphdef, "GPRINT:avg:Average\\: %.2lf";

			push @graphdef, "VDEF:max=${var}-max,MAXIMUM";
			push @graphdef, "GPRINT:max:Maximum\\: %.2lf";

			push @graphdef, "VDEF:cur=${var}-avg,LAST";
			push @graphdef, "GPRINT:cur:Current\\: %.2lf";

			my @dayGraphDef = @graphdef;
			push @dayGraphDef, @{cfgToGraphDef($graphConfig->{'day'}, ${var}, 24)};

			$mtime=(stat("${outdir}/${hostname}.${var}.long.png.work"))[9];
			($averages, $width, $height) =
				rrd_graph("${outdir}/${hostname}.${var}.long.png.work",
						"-w", $graphWidth, @dayGraphDef);

			if ($rrd_result) {
				print "Error while graphing: " . $rrd_result . "\n";
			} else {
				my $newmtime=(stat("${outdir}/${hostname}.${var}.long.png.work"))[9];
				if ((!defined($mtime)) || ($newmtime != $mtime)) {
					copy("${outdir}/${hostname}.${var}.long.png.work", "${outdir}/${hostname}.${var}.long.png.new");
					rename("${outdir}/${hostname}.${var}.long.png.new", "${outdir}/${hostname}.${var}.long.png");
				}
			}

			print HTML2 "<img src=\"${hostname}.${var}.long.png\" width=\"${width}\" height=\"${height}\"><br>\n";

			$mtime=(stat("${outdir}/${hostname}.${var}.week.png.work"))[9];

			my @weekGraphDef = @graphdef;
			push @weekGraphDef, @{cfgToGraphDef($graphConfig->{'week'}, ${var}, 24)};

			($averages, $width, $height) =
				rrd_graph("${outdir}/${hostname}.${var}.week.png.work",
						"-w", "$graphWidth", "-e", "now", "-s", "00:00-8d", @weekGraphDef);

			if ($rrd_result) {
				print "Error while graphing: " . $rrd_result . "\n";
			} else {
				my $newmtime=(stat("${outdir}/${hostname}.${var}.week.png.work"))[9];
				if ((!defined($mtime)) || ($newmtime != $mtime)) {
					copy("${outdir}/${hostname}.${var}.week.png.work", "${outdir}/${hostname}.${var}.week.png.new");
					rename("${outdir}/${hostname}.${var}.week.png.new", "${outdir}/${hostname}.${var}.week.png");
				}
			}

			print HTML2 "<img src=\"${hostname}.${var}.week.png\" width=\"${width}\" height=\"${height}\"><br>\n";

			$mtime=(stat("${outdir}/${hostname}.${var}.year.png.work"))[9];

			my @yearGraphDef = @graphdef;
			push @yearGraphDef, @{cfgToGraphDef($graphConfig->{'year'}, ${var}, 3)};

			($averages, $width, $height) =
				rrd_graph("${outdir}/${hostname}.${var}.year.png.work",
						"-w", "$graphWidth", "-e", "00:00", "-s", "end-365d", @yearGraphDef);

			if ($rrd_result) {
				print "Error while graphing: " . $rrd_result . "\n";
			} else {
				my $newmtime=(stat("${outdir}/${hostname}.${var}.year.png.work"))[9];
				if ((!defined($mtime)) || ($newmtime != $mtime)) {
					copy("${outdir}/${hostname}.${var}.year.png.work", "${outdir}/${hostname}.${var}.year.png.new");
					rename("${outdir}/${hostname}.${var}.year.png.new", "${outdir}/${hostname}.${var}.year.png");
				}
			}

			print HTML2 "<img src=\"${hostname}.${var}.year.png\" width=\"${width}\" height=\"${height}\"><br>\n";

			print HTML2 "</body></html>\n";
			close(HTML2);
			rename("${outdir}/${hostname}.${var}.html.new", "${outdir}/${hostname}.${var}.html");
		}
	}

	print HTML "</body></html>\n";
	print HTML "<br>Generated on: " . localtime(time());
	print HTML ' by <a href="http://git.zerfleddert.de/cgi-bin/gitweb.cgi/upsgraph">upsgraph</a>.';

	close(HTML);

	rename("${outdir}/index.html.new", "${outdir}/index.html");

	sleep(${step}/2);
	$first = 0;
}
